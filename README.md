
## Prepare environment (ALWAYS DO THIS STEP)

MAKE SURE YOU HAVE ANACONDA (do `module load anaconda3` or something else)

```
module load gcc/10.2.0
conda activate /s/ls4/groups/g0130/remd_project/hremd_python
export PS1="(hremd_python) [\u@\h \W] "
source /s/ls4/groups/g0130/remd_project/cp2k/tools/toolchain/install/setup
export PATH=/s/ls4/groups/g0130/remd_project/cp2k/exe/local:$PATH
```

# Create conda env in `./hremd_python`

```
conda create --prefix ./hremd_python python=3.9
```

# Rebuild cp2k

Do NOT do this on the head node (or decrease -j to 8)
Do `prepare environment` steps and then,

```
cd cp2k
make -j 48 ARCH=local VERSION="ssmp sdbg psmp pdbg"
```

# Run cp2k

```
cp2k.ssmp <args>
```
